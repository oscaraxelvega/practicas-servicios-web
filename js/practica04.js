function hacerPeticion(id){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/users/"+id;

    //validar respuesta
    http.onreadystatechange = function(){
        let res = document.getElementById("lista");
        if(this.status == 200 && this.readyState==4){
            //aqui se dibuja la pagina
            res.innerHTML="";
            const json = JSON.parse(this.responseText);
            res.innerHTML += '<tr><td>'+json.id
            +'</td><td>'+json.name
            +'</td><td>'+json.username
            +'</td><td>'+json.email
            +'</td><td>'+json.address.street+", "+json.address.suite+", "+json.address.city
            +'</td><td>'+json.phone
            +'</td></tr>';
        }
        else{
            res.innerHTML="";
            res.innerHTML += '<td>No hay resultados</td><td>No hay resultados</td><td>No hay resultados</td><td>No hay resultados</td><td>No hay resultados</td><td>No hay resultados</td>'
        }
    }
    http.open('GET',url,true);
    http.send();
}

//codificar los botones

document.getElementById('btnCargar').addEventListener("click",function(){
    id = parseInt(document.getElementById('idUsuario').value);
    if(!id){
        alert("Ingrese un ID");
    } else{
        hacerPeticion(id);
    }
});
document.getElementById('btnLimpiar').addEventListener("click",function(){
    let res = document.getElementById('lista');
    res.innerHTML="";
    document.getElementById('idUsuario').value = "";
});