function hacerPeticion(id){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/albums/"+id+"";

    //validar respuesta
    http.onreadystatechange = function(){
        let res = document.getElementById("titulo");
        if(this.status == 200 && this.readyState==4){
            //aqui se dibuja la pagina
            const json = JSON.parse(this.responseText);
            res.value = json.title;
        }
        else{
            res.value = "No hay título con ese id";
        }
    }
    http.open('GET',url,true);
    http.send();
}

//codificar los botones

document.getElementById('btnCargar').addEventListener("click", function(){
    id = parseInt(document.getElementById('idTitulo').value);
    console.log(id);
    if(!id){
        document.getElementById('titulo').value = "";
        alert("Ingrese un ID");
    } else{
        hacerPeticion(id);
    }
});
document.getElementById('btnLimpiar').addEventListener("click",function(){
    document.getElementById('idTitulo').value = "";
    document.getElementById('titulo').value = "";
});