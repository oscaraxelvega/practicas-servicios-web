const canvas = document.getElementById("lava-lamp");
const ctx = canvas.getContext("2d");

const colors = ["#03254c", "#1167b1", "#187bcd", "#2a9df4"]; // Colores de la lámpara de lava

function generateBubbles() {
    const bubbles = [];
    for (let i = 0; i < 50; i++) {
        const x = Math.random() * canvas.width;
        const y = Math.random() * canvas.height;
        const radius = Math.random() * 20 + 5;
        const color = colors[Math.floor(Math.random() * colors.length)];
        const speed = Math.random() * 5 + 1;
        bubbles.push({ x, y, radius, color, speed });
    }
    return bubbles;
}

function drawBubble(bubble) {
    ctx.beginPath();
    ctx.arc(bubble.x, bubble.y, bubble.radius, 0, Math.PI * 2);
    ctx.fillStyle = bubble.color;
    ctx.fill();
}

function animate() {
    requestAnimationFrame(animate);
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (const bubble of bubbles) {
        bubble.y -= bubble.speed;
        if (bubble.y + bubble.radius < 0) {
            bubble.y = canvas.height + bubble.radius;
        }
        drawBubble(bubble);
    }
}

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const bubbles = generateBubbles();
animate();
