function hacerPeticion(){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/users";

    //validar respuesta
    http.onreadystatechange = function(){
        if(this.status == 200 && this.readyState==4){
            //aqui se dibuja la pagina
            let res = document.getElementById("lista");
            res.innerHTML="";
            const json = JSON.parse(this.responseText);

            //CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTROS
            for(const datos of json){
                res.innerHTML += '<tr><td>'+datos.id
                +'</td><td>'+datos.name
                +'</td><td>'+datos.username
                +'</td><td>'+datos.email
                +'</td><td>'+datos.address.street+", "+datos.address.suite+", "+datos.address.city
                +'</td><td>'+datos.phone
                +'</td></tr>';
            }
        }
    }
    http.open('GET',url,true);
    http.send();
}

//codificar los botones

document.getElementById('btnCargar').addEventListener("click",function(){
    hacerPeticion();
});
document.getElementById('btnLimpiar').addEventListener("click",function(){
    let res = document.getElementById('lista');
    res.innerHTML="";
});